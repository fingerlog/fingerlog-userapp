<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js"></script>
    @include('Alerts::alerts')

    <script>
        var fingerlogemail = "";
        var checkStatus = function() {
            $.post('http://dulak.info/fingerlog/public/api/auth/checkaccess', {fingerlogemail: fingerlogemail},function(data) {
                if (data.status == 'notfound') {
                    setTimeout(checkStatus, 2000);
                } else if (data.status == 'failed') {
                    swal("Authentication failed", "Request either too old, or data was incorrect.", "error");
                } else if (data.status == 'finished') {
                    swal("Login response detected", "We have detected a successful login response!", "success");
                }
            });
        };
        $( document ).ready(function() {

            $( "#fingerlog" ).click(function() {
                swal({
                    title: "Fingerlog Authentication",
                    html: '<br />' +
                    '<form method="post" id="fingerlog-authentication" name="fingerlog-authentication">' +
                    '<input type="hidden" name="_token" value="{{ csrf_token() }}">' +
                    '<input id="fingerlog-email" autofocus minlength="5" class="form-control wedding-input-text wizard-input-pad" ' +
                    'type="text" name="fingerlog-email" placeholder="Fingerlog Email" value="' + $('#email').val() + '">' +
                    '</form>',
                    type: "question",
                    showCancelButton: true,
                    animation: "slide-from-top"
                }).then(function() {
                    if ($("#fingerlog-email").val() === false) return false;
                    var emailaddress = $("#fingerlog-email").val();
                    $.get( "fingerlog/" + emailaddress, function( data ) {
                        $( ".result" ).html( data );
                        fingerlogemail = emailaddress;
                        checkStatus();
                        swal("Authentication Request Made!", "Please confirm authentication on your mobile device.", "success");
                    });
                });
            });
        });
    </script>
</body>
</html>
