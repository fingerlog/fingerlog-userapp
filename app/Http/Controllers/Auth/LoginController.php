<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use GuzzleHttp;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function fingerlog($email)
    {
        if($email) {
            $url    = 'http://dulak.info/fingerlog/public/api/auth/notify';
            $client = new GuzzleHttp\Client(['headers' => ['Access-Control-Allow-Origin' => '*']]);
            try {
                $result = $client->request('post', $url, [
                    'form_params' => [
                        'fingerlog-email' => $email
                    ]
                ]);
                var_dump($result->getBody()->getContents());
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                echo $e->getResponse()->getBody()->getContents();
            }
        }
    }
}
